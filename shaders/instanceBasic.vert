#version 330 compatibility

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 offset;
layout(location = 3) in float size;
layout(location = 4) in vec4 orientation;
layout(location = 5) in vec2 uv;

uniform mat4 Projection;
uniform mat4 View;

out vec3 out_normal;
out vec2 out_uv;

#define SQUARED(X)  X*X

void main()
{
    vec3 col1 = vec3 ( 1.f - 2.f * SQUARED ( orientation.y ) - 2.f * SQUARED ( orientation.z ), 
                       2.f * orientation.x * orientation.y + 2.f * orientation.w * orientation.z, 
                       2.f * orientation.x * orientation.z - 2.f * orientation.w * orientation.y );

    vec3 col2 = vec3 ( 2.f * orientation.x * orientation.y - 2.f * orientation.w * orientation.z, 
                       1.f - 2.f * SQUARED ( orientation.x ) - 2.f * SQUARED ( orientation.z ),
                       2.f * orientation.y * orientation.z + 2.f * orientation.w * orientation.x );

    vec3 col3 = vec3 ( 2.f * orientation.x * orientation.z + 2.f * orientation.w * orientation.y,
                       2.f * orientation.y * orientation.z - 2.f * orientation.w * orientation.x, 
                       1.f - 2.f * SQUARED ( orientation.x ) - 2.f * SQUARED ( orientation.y ) );

  mat3 rot_mat = mat3 ( col1, col2, col3 );
  out_normal = rot_mat * normal;

  mat4 model_mat = mat4 ( col1.x, col1.y, col1.z, .0f,
                          col2.x, col2.y, col2.z, .0f,
                          col3.x, col3.y, col3.z, .0f,
                          offset.x, offset.y, offset.z, 1.f );
  mat4 scale = mat4 ( size, .0f, .0f, .0f,
                      .0f, size, .0f, .0f,
                      .0f, .0f, size, .0f,
                      .0f, .0f, .0f, 1.f ); 

  out_uv = uv;
  gl_Position = Projection * View * model_mat * scale * vec4(position,1.0);
}
