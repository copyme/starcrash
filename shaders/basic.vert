#version 330 compatibility

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

uniform mat4 Projection;
uniform mat4 View;
out vec3 out_normal;
out vec2 out_uv;

void main()
{
  out_normal = normal;
  out_uv = uv;
  gl_Position = Projection * View * vec4(position,1.0);
}
