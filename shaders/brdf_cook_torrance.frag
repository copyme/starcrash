#version 330 compatibility

uniform vec4 color;
uniform vec3 camera_position;
uniform mat4 InverseViewProjection;
uniform vec3 lightPos;
uniform vec3 specular_color;
uniform mat4  ProjectionLight;

uniform sampler2D diffuse;
uniform sampler2D Normal;
uniform sampler2D Depth;
uniform sampler2D Shadow;

out vec4 Color;

in vec2 out_uv;

vec2 blinn ( vec3 lightPos,vec3 position, float shininess )
{
   vec3 n = normalize ( texture(Normal, out_uv).rgb );
   vec3 e = normalize ( lightPos - position );
   vec3 l = normalize ( -position );

   float intensity = max ( dot ( n, l ), 0.0 );
   vec2 intensity_specular = vec2(0);
   
   if ( intensity > 0.0 )
   {
    vec3 h = normalize(l + e);
    float spec_angle = max ( dot ( h, n ), 0.0);
    intensity_specular[1] = pow ( spec_angle, shininess );   
   }
   return intensity_specular;
}

vec2 phong ( vec3 lightPos,vec3 position, float shininess, int mode )
{
   vec3 n = normalize ( texture(Normal, out_uv).rgb );
   vec3 e = normalize ( lightPos - position );
   vec3 l = normalize ( -position );
   
   float intensity = max ( dot ( n, l ), 0.0 );
   vec2 intensity_specular = vec2(0);
   
   if ( intensity > 0.0 )
   {
      vec3 r_dir = reflect( -l, n );
      float spec_angle = max ( dot ( r_dir, e ), 0.0 );
      intensity_specular[1] = pow ( spec_angle, shininess );
      
      // according to the rendering equation we would need to multiply
      // with the the "intensity", but this has little visual effect
      if(mode == 1) 
	intensity_specular[1] *= intensity;
      // switch to mode 2 to turn off the specular component
      if(mode == 2) 
	intensity_specular[1] = 0.0;
   }
   return intensity_specular;
}


float Beckmann ( float m, float t )
{
    float M = m * m;
    float T = t * t;
    return exp ( ( T - 1.f ) / ( M * T ) ) / ( M * T * T );
}

float Fresnel(float f0, float u)
{
    return f0 + ( 1.f - f0 ) * pow ( 1.f - u, 5.f );
}

vec2 cook_torrence ( vec3 lightPos,vec3 position, float shininess, bool include_F, bool include_G, float f0 )
{
   vec3 n = normalize ( texture(Normal, out_uv).rgb );
   vec3 e = normalize ( lightPos - position );
   vec3 l = normalize ( -position );
   
   vec3 h = normalize ( l + e );
   float spec_angle = dot ( h, n );
   
   float e_dot_h = max ( dot(e, h), 0.0 );
   float intensity = max ( dot ( n, l ), 0.0 );
   float e_dot_n = dot(n, e);
   float one_over_n_dot_e = 1.0 / e_dot_n;
   
   //output
   vec2 intensity_specular = vec2 ( intensity );

   float D = Beckmann(shininess, spec_angle );
   float F = Fresnel(f0, e_dot_h );
   
   spec_angle = spec_angle + spec_angle;
      
   float G =  0.;
         
   if ( e_dot_n < intensity )
   {
      if ( e_dot_n * spec_angle < e_dot_h )
      {
	G = spec_angle / e_dot_h;  
      }
      else
      {
       G = one_over_n_dot_e;
      }
   }
   else
   {
      if ( intensity * spec_angle < e_dot_h )
      {
	G = spec_angle * intensity / ( e_dot_h * e_dot_n );
      }
      else
      {
	G = one_over_n_dot_e;
      }
   }
               
   if (include_G) 
      G = one_over_n_dot_e;
      
   intensity_specular[1] = spec_angle < 0 ? 0.0 : D * G;
   
   if (include_F) 
       intensity_specular[1] *= F;
   
   intensity_specular[1] /= intensity;
   return intensity_specular;
}

void main()
{

    vec2 poissonDisk[16] = vec2[](
        vec2( -0.94201624, -0.39906216 ),
        vec2( 0.94558609, -0.76890725 ),
        vec2( -0.094184101, -0.92938870 ),
        vec2( 0.34495938, 0.29387760 ),
        vec2( -0.91588581, 0.45771432 ),
        vec2( -0.81544232, -0.87912464 ),
        vec2( -0.38277543, 0.27676845 ),
        vec2( 0.97484398, 0.75648379 ),
        vec2( 0.44323325, -0.97511554 ),
        vec2( 0.53742981, -0.47373420 ),
        vec2( -0.26496911, -0.41893023 ),
        vec2( 0.79197514, 0.19090188 ),
        vec2( -0.24188840, 0.99706507 ),
        vec2( -0.81409955, 0.91437590 ),
        vec2( 0.19984126, 0.78641367 ),
        vec2( 0.14383161, -0.14100790 )
    );

    float shininess = 4; // for cook_torrence_brdf should be less then 1
    vec4 diffuse_color = texture(diffuse, out_uv);
    vec3 shadow = texture(Shadow, out_uv).rgb;
    float ambient = 0.25;
    vec2 intensity_specular = vec2(0);
    
    float depth = texture(Depth, out_uv).r;
    vec2  xy = out_uv * 2.0 -1.0;
    vec4  wPosition =  vec4(xy, depth * 2.0 -1.0, 1.0) * InverseViewProjection;
    vec3  position = vec3(wPosition/wPosition.w);
    
    vec4 wlightSpacePosition = ProjectionLight * vec4 ( position, 1.0 );
    vec3 lightSpacePosition = vec3( wlightSpacePosition / wlightSpacePosition.w );
    
//     intensity_specular = blinn ( lightPos, shininess );
//     intensity_specular = phong ( lightPos, shininess, 0 );
    intensity_specular = cook_torrence ( lightPos,position, 0.3, true, false, .1 );
    vec3 color = (diffuse_color *  max( ambient + intensity_specular[0] * diffuse_color + intensity_specular[1] * vec4(specular_color,1), ambient)).rgb;
    
    float visibility = 1.;
    
    if (wlightSpacePosition.w > 0.0  && 
      lightSpacePosition.x > 0.0 && 
      lightSpacePosition.x < 1.0 && 
      lightSpacePosition.y > 0.0 && 
      lightSpacePosition.y < 1.0 )
      {
        float voffset = 0.7 / 16;
        for (int i=0;i<16;i++)
          if ( texture(Shadow, lightSpacePosition.xy + poissonDisk[i]/1000).r  + 0.0001 < lightSpacePosition.z  )
            visibility -= voffset;
      }        
    Color = vec4(color.rgb * visibility,diffuse_color.a); // use alpha from diffuse to remove light on env map
}
