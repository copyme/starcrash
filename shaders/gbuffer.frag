#version 330 compatibility

uniform sampler2D Diffuse;
uniform sampler2D Spec;

in vec2 out_uv;
in vec3 out_normal;

out vec4  Color;
out vec4  Normal;

void main(void)
{
	vec3 diffuse = texture(Diffuse, out_uv).rgb;
// 	float spec = texture(Spec, uv).r;
	Color = vec4(diffuse, 1);
	Normal = vec4(out_normal, 0);
}