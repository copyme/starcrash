#version 330 core
// For unit square only.
layout(location = 0) in vec2 position;


uniform mat4 MVP;

out vec2 out_uv;

void main()
{
  out_uv.xy = position.xy;
  gl_Position = MVP * vec4 ( position, .0f, 1.f );
}
