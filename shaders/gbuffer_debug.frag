#version 330 compatibility

in vec2 out_uv;
uniform sampler2D Texture1;
uniform sampler2D Env;

out vec4  Color;

void main(void)
{
	vec4 colorObj = texture(Texture1, out_uv).rgba;
	vec4 colorEnv = texture(Env, out_uv).rgba;
	if ( colorObj.a != 0.f )
	  Color = colorObj;
	else
	  Color = colorEnv;
}