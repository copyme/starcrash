#version 330

layout (location = 0) in vec3  position;
layout (location = 1) in vec3  direction; // Assumed to be normalized
layout (location = 2) in float speed;
layout (location = 3) in float size;

out vec3 new_position; // To transform feedback
out vec3 new_direction;
out float new_speed;
out float new_size;

uniform float timestep;

void main ( void )
{
	vec3 vel = speed * direction;
	new_position = position + timestep * vel;
	new_direction = direction;
	new_speed = speed;
	new_size = size;
}