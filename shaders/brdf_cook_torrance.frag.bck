#version 330 compatibility

uniform vec4 color;
uniform vec3 camera_position;
in vec3 out_normal;

vec2 blinn ( vec3 lightPos, float shininess )
{
   vec3 n = normalize ( out_normal );
   vec3 e = normalize ( camera_position );
   vec3 l = normalize ( lightPos );

   float intensity = max ( dot ( n, l ), 0.0 );
   vec2 intensity_specular = vec2(0);
   
   if ( intensity > 0.0 )
   {
    vec3 h = normalize(l + e);
    float spec_angle = max ( dot ( h, n ), 0.0);
    intensity_specular[1] = pow ( spec_angle, shininess );   
   }
   return intensity_specular;
}

vec2 phong ( vec3 lightPos, float shininess, int mode )
{
   vec3 n = normalize ( out_normal );
   vec3 e = normalize ( camera_position );
   vec3 l = normalize ( lightPos );
   
   float intensity = max ( dot ( n, l ), 0.0 );
   vec2 intensity_specular = vec2(0);
   
   if ( intensity > 0.0 )
   {
      vec3 r_dir = reflect( -l, n );
      float spec_angle = max ( dot ( r_dir, e ), 0.0 );
      intensity_specular[1] = pow ( spec_angle, shininess );
      
      // according to the rendering equation we would need to multiply
      // with the the "intensity", but this has little visual effect
      if(mode == 1) 
	intensity_specular[1] *= intensity;
      // switch to mode 2 to turn off the specular component
      if(mode == 2) 
	intensity_specular[1] = 0.0;
   }
   return intensity_specular;
}


float Beckmann ( float m, float t )
{
    float M = m * m;
    float T = t * t;
    return exp ( ( T - 1 ) / ( M * T ) ) / ( M * T * T );
}

float Fresnel(float f0, float u)
{
    return f0 + ( 1 - f0 ) * pow ( 1 - u, 5 );
}

vec2 cook_torrence ( vec3 lightPos, float shininess, bool include_F, bool include_G, float f0 )
{
   vec3 n = normalize ( out_normal );
   vec3 e = normalize ( camera_position );
   vec3 l = normalize ( lightPos );
   
   vec3 h = normalize ( l + e );
   float spec_angle = dot ( h, n );
   
   float e_dot_h = max ( dot(e, h), 0.0 );
   float intensity = max ( dot ( n, l ), 0.0 );
   float e_dot_n = dot(n, e);
   float one_over_n_dot_e = 1.0 / e_dot_n;
   
   //output
   vec2 intensity_specular = vec2 ( intensity );

   float D = Beckmann(shininess, spec_angle );
   float F = Fresnel(f0, e_dot_h );
   
   spec_angle = spec_angle + spec_angle;
      
   float G =  0.;
         
   if ( e_dot_n < intensity )
   {
      if ( e_dot_n * spec_angle < e_dot_h )
      {
	G = spec_angle / e_dot_h;  
      }
      else
      {
       G = one_over_n_dot_e;
      }
   }
   else
   {
      if ( intensity * spec_angle < e_dot_h )
      {
	G = spec_angle * intensity / ( e_dot_h * e_dot_n );
      }
      else
      {
	G = one_over_n_dot_e;
      }
   }
               
   if (include_G) 
      G = one_over_n_dot_e;
      
   intensity_specular[1] = spec_angle < 0 ? 0.0 : D * G;
   
   if (include_F) 
       intensity_specular[1] *= F;
   
   intensity_specular[1] /= intensity;
   return intensity_specular;
}


void main()
{
    float shininess = 4; // for cook_torrence_brdf should be less then 1
    vec4 specular_color = vec4 ( 0.,0.5,0.5,0 );
    vec4 diffuse_color = vec4 ( 0.25,0.25,0.25,1 );
    float ambient = 0.25;
    vec3 lightPos = vec3 ( 1.0,1.0,1.0 );
    vec2 intensity_specular = vec2(0);
//     intensity_specular = blinn ( lightPos, shininess );
//     intensity_specular = phong ( lightPos, shininess, 0 );
    intensity_specular = cook_torrence ( lightPos, 0.3, true, false, 0.1 );
    gl_FragColor = color *  max( ambient + intensity_specular[0] * diffuse_color + intensity_specular[1] * specular_color, ambient );
}
