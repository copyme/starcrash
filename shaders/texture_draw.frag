#version 330 core

in vec2 out_uv;
uniform sampler2D Texture1;

out vec4  Color;

void main(void)
{
	Color = vec4 ( texture(Texture1, out_uv).rgb, 1.f );
}