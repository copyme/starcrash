// Note this only work for directionnal lights.

#version 330 core

in vec2 out_uv;
uniform sampler2D z_depth;
uniform sampler2D lights;

out vec4  Color;

void main(void)
{
    if ( texture ( z_depth, out_uv ).r < 0.999999f ) // Occluded objects.
    {
        Color = vec4 ( .0f, .0f, .0f, 1.f );
    }
    else
    {
        Color = texture ( lights, out_uv );
    }
}