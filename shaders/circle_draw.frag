#version 330 core

in vec2 out_uv;

out vec4 Color;

void main(void)
{
	float dx = out_uv.x;
	float dy = out_uv.y;
	float dist =  dx * dx + dy * dy;

	Color = mix ( vec4 ( 1.f ), vec4 ( .0f, .0f, .0f, 1.f ),
		         smoothstep ( .05f, 0.1f, dist ) );
}