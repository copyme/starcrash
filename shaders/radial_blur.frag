#version 330 core
in vec2 out_uv;
uniform float texture_ratio;
uniform vec3  shaft_color1;
uniform vec2  lightOnScreen;

uniform sampler2D firstPass;

out vec4  Color;

#define SAMPLE_COUNT 64
#define INV_SAMPLE_COUNT 1.f / SAMPLE_COUNT
void main(void)
{   
	float weight = 1.f;
	float decay = .00075f;
	float intensity = .25f;
	vec4 color = vec4 ( .0f );
	vec2 lightPositionOnScreen = lightOnScreen;

	// Do a radial blur, with the center as the light's center in screen space
	vec2 blur_direction = ( lightPositionOnScreen - out_uv );
	// As our screen is not always a square, we should compensate to avoid blurring
	// too much in the shorter dimension
	blur_direction.y *= texture_ratio; 
	
	vec2 texCoord = out_uv;
	blur_direction *= INV_SAMPLE_COUNT;

	for (int s = 0; s < SAMPLE_COUNT; ++s)
	{
		float weight = 1.0f - s * decay;

		color += texture ( firstPass, texCoord ) * weight;
		
		texCoord += blur_direction;
	} 

	Color =  vec4 ( mix ( color.rgb * ( INV_SAMPLE_COUNT * intensity ), shaft_color1, color.r * ( INV_SAMPLE_COUNT * intensity ) ) , 1.f );
}

