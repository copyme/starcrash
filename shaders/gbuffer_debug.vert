#version 330 compatibility

layout(location = 0) in vec2 position;

out vec2 out_uv;

void main(void)
{
	out_uv = position * 0.5 + 0.5;
	gl_Position = vec4(position.xy, 0.0, 1.0);
}