
include_directories(${QT_INCLUDES} ${CMAKE_CURRENT_BINARY_DIR})
set(CMAKE_EXECUTABLE_OUTPUT_PATH ${CMAKE_CURRENT_BINARY_DIR})
set(UnitTestsTest_SRCS UnitTestsTest.cpp)
qt4_automoc(${UnitTestsTest_SRCS})
add_executable(UnitTestsTest ${UnitTestsTest_SRCS})
add_test(UnitTestsTest UnitTestsTest)
target_link_libraries(UnitTestsTest ${QT_QTCORE_LIBRARY} ${QT_QTTEST_LIBRARY})
