#ifndef UnitTestsTEST_H
#define UnitTestsTEST_H

#include <QtCore/QObject>

class UnitTestsTest : public QObject
{
Q_OBJECT
private slots:
    void initTestCase();
    void init();
    void cleanup();
    void cleanupTestCase();

    void someTest();
};

#endif // UnitTestsTEST_H
