#include <QCoreApplication>
#include "UnitTests.h"


int main(int argc, char** argv)
{
    QCoreApplication app(argc, argv);
    UnitTests foo;
    return app.exec();
}
