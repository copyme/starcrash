#include <vector>
#include <iostream>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <map>
#include "interface.h"
#include "indexed_mesh.hpp"
#include <core/globals.h>
#include "helpers/lightfactory.h"
#include "helpers/objectfactory.h"

int main(void)
{
    std::ifstream f_objects("objects.conf");
    std::map < std::string,  TexturedMesh * > obj_register; // memory released by Interface
    ObjectFactory obj_factory;
    
    std::ifstream f_lights ("lights.conf");
    std::vector < Light > lights_reg;
    LightFactory light_factory;
    light_factory.get( f_lights, lights_reg );
    f_lights.close();
    
    Interface ui;
    ui.init();
    
    obj_factory.get ( f_objects, obj_register );
    f_objects.close();
    
    ui.set_objects ( &obj_register );
    ui.set_lights( &lights_reg );
    
    return ui.exec();
}