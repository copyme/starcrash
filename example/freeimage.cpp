/** \author Kacper Pluta
 * Demostration how to use FreeImagePlus
 * More info: http://freeimage.sourceforge.net/fip/classfipImage.html
 * 
 * Table of content:
 * - Loading image
 * - Getting image information
 * -- Width
 * -- Height
 * - Checking image color information
 * - Safe reading of data
 * - RAW data access 
 * 
 */

#include <iostream>
#include <stdexcept>
#include <FreeImagePlus.h>
#include <string.h>

int main ( int argc, char ** argv )
{
    fipImage image;
    if ( image.load( argv[1], 0) == false )
        throw std::runtime_error ( "Image could not be load" );

    std::cout << "The size of the bitmap is the BITMAPINFOHEADER + the size of the palette + the size of the bitmap data: " << image.getImageSize() << std::endl;
    std::cout << "Image W x H: " << image.getWidth() << " " << image.getHeight() << std::endl;

    //is color
    if ( image.getColorType() == 2 )
        std::cout << "Image is encoded in RGB " << std::endl;
    else if ( image.isGrayscale() )
        std::cout << "Image is encoded in Grayscale " << std::endl;

    /*
     *
     * Safe access to the RGB
     *
     */

    if ( image.getColorType() == 2 )
        for ( unsigned int i = 0; i < 10; i++ )
        {
            for ( unsigned int j = 0; j < 10; j++ )
            {
                RGBQUAD value;
                image.getPixelColor(i,j, &value);
                std::cout << "Value for X " << i << " Y " << j << " R " << (int)value.rgbRed << " G " << (int)value.rgbGreen << " B " << (int)value.rgbBlue << std::endl;
            }
        }

    /*
     *
     * Safe access to the graydata
     *
     */

    else if ( image.isGrayscale() )
        for ( unsigned int i = 0; i < 10; i++ )
        {
            for ( unsigned int j = 0; j < 10; j++ )
            {
                BYTE value;
                image.getPixelIndex(i,j, &value);
                std::cout << "Value for X" << i << " Y " << j << " value " << (int)value << std::endl;
            }
        }
        
   /*
    * 
    *  Accessing RAW data
    * 
    */
   if ( image.getColorType() == 2 )
   { 
     unsigned int dataSize = sizeof(unsigned char) * image.getHeight() * image.getWidth() * 3; //getImageSize() return value with something extra
     unsigned char *data = (unsigned char * ) malloc( dataSize );
     if ( data == nullptr )
       throw std::runtime_error ( "Memory allocation faild!" );
     memcpy (data, image.accessPixels(), dataSize );
     
     free( data );
   }
   
  // Save image 
   image.save (  "test2.tif", 0 );
   
    return 0;
}