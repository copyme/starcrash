#ifndef INTERFACE_H
#define INTERFACE_H

#include <map>
#include <list>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include "texturedmesh.h"
#include "tiny_obj_loader.h"
#include "camera.h"
#include "statemonitor.h"
#include <glslprogram.h>
#include <core/gbuffermanager.h>

class Interface
{
private:
    GLFWwindow * window;
    GLFWvidmode const * screen;
    std::map< std::string, TexturedMesh * > * objects_register;
    std::vector < Light > * lights;
    GBufferManager gbuffer_man;
    GUIStateMonitor guiStates;
    void mouse_button_callback(GLFWwindow * window, int button, int action, int mods);
public:
    Interface();
    void init();
    //! This has to be called after init()!!!
    void set_objects ( std::map< std::string, TexturedMesh * > * objects_register ) { this->objects_register = objects_register; }
    void set_lights ( std::vector < Light > * lights ) { this->lights  =lights; }
    int exec ();
    ~Interface();
};

#endif // INTERFACE_H
