#include "input.h"

void Input::mouse_button_impl ( GLFWwindow * window, Camera & camera )
{
    // Mouse states
    int leftButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_LEFT );
    int rightButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_RIGHT );
    int middleButton = glfwGetMouseButton( window, GLFW_MOUSE_BUTTON_MIDDLE );
    int Akey = glfwGetKey( window, GLFW_KEY_A );// For the qwerty
    int Qkey = glfwGetKey( window, GLFW_KEY_Q );// For the azerty
    int Dkey = glfwGetKey( window, GLFW_KEY_D );// For everyone
    int Wkey = glfwGetKey( window, GLFW_KEY_W );// For the qwerty
    int Zkey = glfwGetKey( window, GLFW_KEY_Z );// For the azerty
    int Skey = glfwGetKey( window, GLFW_KEY_S );// For everyone
    int leftArrow = glfwGetKey( window, GLFW_KEY_LEFT );
    int rightArrow = glfwGetKey( window, GLFW_KEY_RIGHT );
    int upArrow = glfwGetKey( window, GLFW_KEY_UP );
    int downArrow = glfwGetKey( window, GLFW_KEY_DOWN );

    if( leftButton == GLFW_PRESS )
      guiStates.lock_turn();
    else
      guiStates.unlock_turn();
    
    if( rightButton == GLFW_PRESS )
      guiStates.lock_zoom();
    else
      guiStates.unlock_zoom();
    
    if( middleButton == GLFW_PRESS )
      guiStates.lock_pan();
    else
      guiStates.unlock_pan();
    
    // Camera movements
    int altPressed = glfwGetKey( window, GLFW_KEY_LEFT_SHIFT );
    if (!altPressed && (leftButton == GLFW_PRESS || rightButton == GLFW_PRESS || middleButton == GLFW_PRESS))
    {
      double x; double y;
      glfwGetCursorPos(window,&x, &y);
      guiStates.set_position_x( x );
      guiStates.set_position_y( y );
    }
    
    if (altPressed == GLFW_PRESS)
    {
      double mousex; double mousey;
      glfwGetCursorPos(window,&mousex, &mousey);
      double diffLockPositionX = mousex - guiStates.get_position_x();
      double diffLockPositionY = mousey - guiStates.get_position_y();
      if (guiStates.zoom())
      {
      	float zoomDir = 0.0;
      	if (diffLockPositionX > 0.)
      	  zoomDir = -1.f;
      	else if (diffLockPositionX < 0. )
      	  zoomDir = 1.f;
      	camera.zoom(zoomDir);
      }
      else if (guiStates.turn())
      {
	       camera.turn(diffLockPositionY, diffLockPositionX);
      }
      else if (guiStates.pan())
      {
	       camera.pan(diffLockPositionX, diffLockPositionY);
      }
      guiStates.set_position_x ( mousex );
      guiStates.set_position_y ( mousey );
    }

    if( Qkey == GLFW_PRESS || Akey == GLFW_PRESS || leftArrow == GLFW_PRESS) {
      camera.moveLeft(0.02f);
    }
    if( Dkey == GLFW_PRESS || rightArrow == GLFW_PRESS) {
      camera.moveRight(0.02f);
    }
    if( Zkey == GLFW_PRESS || Wkey == GLFW_PRESS || upArrow == GLFW_PRESS) {
      camera.moveUp(0.02f);
    }
    if( Skey == GLFW_PRESS || downArrow == GLFW_PRESS) {
      camera.moveDown(0.02f);
    }
}