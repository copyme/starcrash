#include "interface.h"
#include <stdexcept>
#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/constants.hpp>
#include "input.h"
#include <glslprogram.h>
#include <core/PCloud.h>
#include <core/environment.h>
#include <core/unitplane.h>
#include <glutils.h>
#include "gltexture.h"
#include <core/post_processor.h>


static void remove_objects ( std::pair< std::string, TexturedMesh * > mesh )
{
  TexturedMesh * remove = mesh.second;
  delete remove;
}

Interface::Interface()
{
  objects_register = nullptr;
  if ( !glfwInit() )
  {
    throw std::runtime_error ( "Main GUI system could not be initiated!" );
  }
  
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, OPENGL_MAJOR_VERSION);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, OPENGL_MINOR_VERSION);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_SAMPLES, 4);
  GLFWmonitor * monitor = glfwGetPrimaryMonitor ();
  if ( monitor == NULL )
  {
    throw std::runtime_error ( "Could not get information about primary monitor!" );
  }
  screen = glfwGetVideoMode ( monitor );
}

void Interface::init ()
{
  /* Create a windowed mode window and its OpenGL context */
  window = glfwCreateWindow ( 1280, 720, "StarCrash",NULL /*glfwGetPrimaryMonitor()*/, NULL );
  if ( !window )
  {
    glfwTerminate ();
    throw std::runtime_error ( "Window could not be created!" );
  }
  // Center window
  glfwSetWindowPos(window, ( 1280 - ( 1280 / 2 ) ) / 2, (720 - ( 720 / 2 ) ) / 2 );
  /* Make the window's context current */
  glfwMakeContextCurrent ( window );
  
  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode ( window, GLFW_STICKY_KEYS, GL_TRUE );
  // Enable vertical sync (on cards that support it)
  glfwSwapInterval ( 1 );
  
  glfwSetWindowSizeCallback ( window, &Input::window_size_callback );
  
  glewExperimental = GL_TRUE;
  if ( GLEW_OK != glewInit() )
  {
    throw std::runtime_error ( "The OpenGL support library initialization faild!" );
  }
  
  gbuffer_man.init( 1280, 720 );
}

int Interface::exec ()
{
  static GLSLProgram shader = []()
  {
    GLSLProgram shader;
    bool status;
    status =  shader.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
    status &= shader.compileShaderFromFile ( "shaders/texture_draw.frag" , GLSLShader::FRAGMENT );
    
    if ( !status )
    {
      throw std::runtime_error ( "Compilation of shaders faild: " + shader.log ( ) );
    }
    
    shader.link ( );
    if ( shader.isLinked ( ) == false )
    {
      throw std::runtime_error ( "Linking of shaders faild: " + shader.log ( ) );
    }
    
    return shader;
  } ( );
  
  //camera.eye = glm::vec3(1.7f, 2.4f, 2.1f);
  //camera.o = glm::vec3(-10.0f, 0.8f, 2.2f);;
  //camera.right = glm::vec3(10.0f, 10.0f, 5.0f);;

  Post_processor post_process { 1280, 720 };
  
  Environment env { "texture/sky.tif"};
  UnitPlane render_plane;
  Environment planet("texture/planet_diff.tif", 5000.f, glm::vec3( -350.f, -2000.f, -2000.f ));

  gbuffer_man.set_geometry(objects_register);
  gbuffer_man.set_lights ( lights );
  gbuffer_man.set_envmap ( &env );
  
  /* Loop until the user closes the window */
  while ( !glfwWindowShouldClose ( window ) )
  {
    if ( glfwGetKey ( window, GLFW_KEY_ESCAPE ) == GLFW_PRESS )
      break;
    

    glEnable(GL_CULL_FACE);
    //Interaction with mouse need to be trigged for each frame -- so we can not use callbacks
    Input::mouse( window, camera );

	

    /* Blender like background color */
    glClearColor ( 0.2235f, 0.2235f, 0.2235f, 0.0f );
    glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
	int width, height;
	glfwGetWindowSize(window, &width, &height);

	gbuffer_man.geometry_pass();
// 	gbuffer_man.shadowmap_pass();
	gbuffer_man.light_pass();
	gbuffer_man.envmap_pass();
	gbuffer_man.deactivate();


	post_process.set_beauty_pass(gbuffer_man.get_render());
	post_process.set_bg_pass(gbuffer_man.get_envmap_render());
	post_process.set_z_pass(gbuffer_man.get_Z());
	post_process.add_planetbackground(&planet);
	post_process.add_environment();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	shader.use();
	shader.setUniform("Texture1", 0);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	glDisable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, post_process.get_output_texture());
	render_plane.draw();
	post_process.add_light_shaft(glm::vec3{ .0f });

	glEnable(GL_BLEND);
	//glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA,GL_ONE,GL_ONE_MINUS_SRC_ALPHA);
	glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
	glBlendFunc(GL_SRC_ALPHA, GL_SRC_ALPHA);
	render_plane.draw();

	glDisable(GL_BLEND);
	glEnable(GL_DEPTH_TEST);

	glfwSwapBuffers(window);
	glfwPollEvents();
  }

  glfwTerminate();
  return 0;
}

Interface::~Interface()
{
  std::for_each ( objects_register->begin(), objects_register->end(), remove_objects );
}
