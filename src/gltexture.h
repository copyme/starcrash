

#ifndef GLTEXTURE_H
#define GLTEXTURE_H

#include <FreeImagePlus.h>
#include <GL/glew.h>
#include <GL/gl.h>

class GLTexture
{
private:
  fipImage  * texture;
  GLuint gl_texture;
  GLenum gltexture_id;
public:
  GLTexture();
  /** \brief Load of texture from hard drive.
   * 
   * If \param preinit true then if texture is RGB function glTexImage2D() is called.
   * 
   */
  void load ( const char * filename );
  void init ( GLuint gl_textNumber );
  GLuint get_gl_texture () const { return gl_texture; }
  /**
   * Texture will be bind to the actual target
   */
  void release_host_memory ();
  void set_id ( GLenum  id ) { gltexture_id = id;  }
  void bind()
  {
    glActiveTexture ( gltexture_id );
    glBindTexture ( GL_TEXTURE_2D, gl_texture );
  }
  ~GLTexture();
};

#endif // GLTEXTURE_H
