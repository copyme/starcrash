/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#include <stdexcept>
#include <cstdlib>
#include "objectfactory.h"
#include <core/PCloud.h>
#include <tiny_obj_loader.h>

static GLenum decode_texture ( int number )
{
  switch ( number )
  {
    case 0:
      return GL_TEXTURE0;
    case 1:
      return GL_TEXTURE1;
    default:
      throw std::runtime_error ( "Wrong texture number! Fix ObjectFactory by adding new one!" );
  }
  
}

static Birth_param get_birthset ( std::string setname )
{
  if ( setname == "BIRTH_SET0" )
  {
    Birth_param bparam;
    bparam.speed           = .07f;
    bparam.speed_variation = .005f;
    bparam.direction_of_travel = glm::vec3 { .0f, .10f, -.5f };
    bparam.direction_variation = glm::vec3 { 10.f };
    bparam.size = .1f;
    bparam.size_variation = .05f;
    bparam.spin_amount = 10.f;
    bparam.spin_variation = 5.f;
    bparam.spin_axis_variation = { 10.f, 20.f, 10.f };
    return bparam;
  }
  else if ( setname == "BIRTH_SET1"  )
  {
      Birth_param bparam;
    bparam.speed           = .1f;
    bparam.speed_variation = .01f;
    bparam.direction_of_travel = glm::vec3 { -0.7f, .10f, -.5f };
    bparam.direction_variation = glm::vec3 { 10.f };
    bparam.size = .6f;
    bparam.size_variation = .09f;
    bparam.spin_amount = 10.f;
    bparam.spin_variation = 5.f;
    bparam.spin_axis_variation = { 10.f, 20.f, 10.f };
    return bparam;
  }
  else if ( setname == "BIRTH_SET2"  )
  {
      Birth_param bparam;
    bparam.speed           = .09f;
    bparam.speed_variation = .05f;
    bparam.direction_of_travel = glm::vec3 { .0f, .10f, -.55f };
    bparam.direction_variation = glm::vec3 { 15.f };
    bparam.size = 1.f;
    bparam.size_variation = .09f;
    bparam.spin_amount = 7.f;
    bparam.spin_variation = 2.f;
    bparam.spin_axis_variation = { 10.f, 20.f, 10.f };
    return bparam;
  }
  else
    throw std::runtime_error ( "Wrong birth set name! Fix ObjectFactory by adding new one!" );
    
}

void ObjectFactory::get ( std::istream & csv, std::map< std::string,  TexturedMesh * > & obj_reg )
{
  std::string line;
  while ( std::getline ( csv, line ) ) 
  {
    tokens.input ( line );
    if ( tokens.getValue ( 0 ) == "OBJECT" && tokens.getSize() >= 3 )
    {
      create_object ( obj_reg );
    }
    if ( tokens.getValue ( 0 ) == "TEXTURE" && tokens.getSize() >= 4 )
    {
      load_texture ( obj_reg );
    }
    if ( tokens.getValue ( 0 ) == "OBJ_GROUP"  && tokens.getSize() >= 2 )
    {
      add_group( obj_reg );
    }
    if ( tokens.getValue ( 0 ) == "OBJ_GROUP2"  && tokens.getSize() >= 2 )
    {
      add_group2( obj_reg );
    }
    if ( tokens.getValue ( 0 ) == "OBJ_GROUP3"  && tokens.getSize() >= 2 )
    {
      add_group3( obj_reg );
    }
    if ( tokens.getValue ( 0 ) == "OBJ_GROUP4"  && tokens.getSize() >= 2 )
    {
      add_group4( obj_reg );
    }    
  }
}

void ObjectFactory::create_object ( std::map< std::string,  TexturedMesh * >& obj_reg )
{
  TexturedMesh * mesh = new TexturedMesh();
  std::vector< tinyobj::shape_t > shapes;
  load_geometry( tokens.getValue ( 2 ), shapes );
  mesh->add_geometry ( shapes );
  obj_reg.insert ( std::pair < std::string,  TexturedMesh * > ( tokens.getValue ( 1 ), mesh ) );
}

void ObjectFactory::load_geometry ( std::string path, std::vector< tinyobj::shape_t > & shapes )
{
    std::string err = tinyobj::LoadObj ( shapes, path.c_str(), NULL);
    if ( err != std::string("") )
        throw std::runtime_error ( err );
}

void ObjectFactory::load_texture( std::map< std::string,  TexturedMesh * >& obj_reg )
{
  std::map < std::string, TexturedMesh * >::iterator object = obj_reg.find( std::string( tokens.getValue ( 1 ) ) );
  if ( object != obj_reg.end() )
  {
    int texture = std::atoi ( tokens.getValue( 2 ).c_str() );
    object->second->add_texture ( tokens.getValue( 3 ).c_str() , decode_texture ( texture ) );
  }
  else
    throw std::runtime_error ( "You want to set texture for no defined mesh!" );
}
void ObjectFactory::add_group ( std::map< std::string, TexturedMesh* >& obj_reg )
{
  AABB bb = { glm::vec3 { -10.f, -20.f, -20.f}, glm::vec3 { 20.f, 20.f, 40.f} };
  PCloud * pcloud = new PCloud();
  int numberOf = std::atoi ( tokens.getValue( 2 ).c_str() );
  Birth_param bparam = get_birthset ( tokens.getValue ( 3 ) );
  
  pcloud->add_group ();
  pcloud->generate_particles ( numberOf, bb, bparam );
  pcloud->updateGL ( );
  pcloud->set_group_shape ( tokens.getValue ( 4 ).c_str(), nullptr ); 
  pcloud->updateGL ( );
  
  TexturedMesh * mesh = new TexturedMesh();
  mesh->add_mesh ( pcloud );
  obj_reg.insert ( std::pair< std::string, TexturedMesh * > ( tokens.getValue( 1 ).c_str(), mesh ) );
}

void ObjectFactory::add_group2 ( std::map< std::string, TexturedMesh* >& obj_reg )
{
  AABB bb = { glm::vec3 { -10.f, -20.f, -20.f}, glm::vec3 { 20.f, 20.f, 40.f} };
  PCloud * pcloud = new PCloud();
  int numberOf = std::atoi ( tokens.getValue( 2 ).c_str() );
  Birth_param bparam = get_birthset ( tokens.getValue ( 3 ) );
  
  pcloud->add_group ();
  pcloud->generate_particles ( numberOf, bb, bparam );
  pcloud->updateGL ( );
  pcloud->set_group_shape ( tokens.getValue ( 4 ).c_str(), nullptr ); 
  pcloud->updateGL ( );
  
  TexturedMesh * mesh = new TexturedMesh();
  mesh->add_mesh ( pcloud );
  obj_reg.insert ( std::pair< std::string, TexturedMesh * > ( tokens.getValue( 1 ).c_str(), mesh ) );
}

void ObjectFactory::add_group3 ( std::map< std::string, TexturedMesh* >& obj_reg )
{
  AABB bb = { glm::vec3 { -10.f, -20.f, -20.f}, glm::vec3 { 20.f, 20.f, 40.f} };
  PCloud * pcloud = new PCloud();
  int numberOf = std::atoi ( tokens.getValue( 2 ).c_str() );
  Birth_param bparam = get_birthset ( tokens.getValue ( 3 ) );
  
  pcloud->add_group ();
  pcloud->generate_particles ( numberOf, bb, bparam );
  pcloud->updateGL ( );
  pcloud->set_group_shape ( tokens.getValue ( 4 ).c_str(), nullptr ); 
  pcloud->updateGL ( );
  
  TexturedMesh * mesh = new TexturedMesh();
  mesh->add_mesh ( pcloud );
  obj_reg.insert ( std::pair< std::string, TexturedMesh * > ( tokens.getValue( 1 ).c_str(), mesh ) );
}

void ObjectFactory::add_group4 ( std::map< std::string, TexturedMesh* >& obj_reg )
{
  AABB bb = { glm::vec3 { -2.f, -2.f, -2.f}, glm::vec3 { 2.f, 4.f, 2.f} };
  PCloud * pcloud = new PCloud();
  int numberOf = std::atoi ( tokens.getValue( 2 ).c_str() );
  Birth_param bparam = get_birthset ( tokens.getValue ( 3 ) );
  
  pcloud->add_group ();
  pcloud->generate_particles ( numberOf, bb, bparam );
  pcloud->updateGL ( );
  pcloud->set_group_shape ( tokens.getValue ( 4 ).c_str(), nullptr ); 
  pcloud->updateGL ( );
  
  TexturedMesh * mesh = new TexturedMesh();
  mesh->add_mesh ( pcloud );
  obj_reg.insert ( std::pair< std::string, TexturedMesh * > ( tokens.getValue( 1 ).c_str(), mesh ) );
}
