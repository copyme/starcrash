/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#include <stdexcept>
#include "lightfactory.h"


void LightFactory::get(std::istream& csv, std::vector< Light >& ligh_reg)
{
  std::string line;
  while ( std::getline ( csv, line ) ) 
  {
    tokens.input ( line );
    if( tokens.getSize() == 8 )
    {
      Light light;
      if( tokens.getValue ( 0 ) == "POINT" )
	light.type = Light::POINT;
      else
	throw std::runtime_error ( "Type of light not supported!" );
      
      float x = std::atof ( tokens.getValue ( 1 ).c_str() );
      float y = std::atof ( tokens.getValue ( 2 ).c_str() );
      float z = std::atof ( tokens.getValue ( 3 ).c_str() );
      
      light.position = { x, y, z };
      
      float r = std::atof ( tokens.getValue ( 4 ).c_str() );
      float g = std::atof ( tokens.getValue ( 5 ).c_str() );
      float b = std::atof ( tokens.getValue ( 6 ).c_str() );
      
      light.color = { r, g, b };
      
      light.intensity = std::atof ( tokens.getValue ( 7 ).c_str() );
      
      ligh_reg.push_back ( light );
    }
    else
      throw std::runtime_error ( "Light wrongly defined!" );
  }
}
