/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#ifndef OBJECTFACTORY_H
#define OBJECTFACTORY_H

#include <map>
#include <core/globals.h>
#include "CSVExtractor.h"
#include <texturedmesh.h>

class ObjectFactory
{
private:  
  CSVExtractor tokens;
  void create_object ( std::map < std::string,  TexturedMesh * > & obj_reg );
  void load_geometry ( std::string path, std::vector< tinyobj::shape_t > & shapes );
  void load_texture ( std::map< std::string,  TexturedMesh * >& obj_reg );
  void add_group ( std::map< std::string,  TexturedMesh * >& obj_reg );
  void add_group2 ( std::map< std::string,  TexturedMesh * >& obj_reg );
  void add_group3 ( std::map< std::string,  TexturedMesh * >& obj_reg );
  void add_group4 ( std::map< std::string,  TexturedMesh * >& obj_reg );
public:
  void get( std::istream & csv, std::map < std::string, TexturedMesh * > & obj_reg );
};

#endif // OBJECTFACTORY_H
