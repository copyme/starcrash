#ifndef LIGHTFACTORY_H
#define LIGHTFACTORY_H
#include <vector>
#include <iostream>
#include <core/light.h>
#include <helpers/CSVExtractor.h>

class LightFactory
{
private:
  CSVExtractor tokens;
public:
  void get( std::istream & csv, std::vector < Light > & ligh_reg );
};

#endif // LIGHTFACTORY_H
