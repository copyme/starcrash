
#ifndef INPUT_H
#define INPUT_H

#include <GLFW/glfw3.h>
#include "camera.h"
#include "statemonitor.h"

class Input
{
private:
    GUIStateMonitor guiStates;
    Input() {}
    void window_size_impl ( int width, int height )
    {
      glViewport( 0, 0, width, height );
    }
    
    void mouse_button_impl ( GLFWwindow * window, Camera & camera );
public:
    static Input & instance()
    {
        static Input _instance;
        return _instance;
    }

    static void window_size_callback ( GLFWwindow* window, int width, int height )
    {
      instance().window_size_impl ( width, height );
    }
    
    static void mouse ( GLFWwindow * window, Camera & camera ) 
    {
      instance().mouse_button_impl ( window, camera );
    }
};

#endif // INPUT_H
