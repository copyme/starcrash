#include <cassert>
#include <stdexcept>
#include "gltexture.h"


GLTexture::GLTexture()
{
  glGenTextures ( 1, &gl_texture );
  texture = 0;
}


void GLTexture::load ( const char * filename )
{
  delete texture;
  texture = new fipImage;
  
  if ( texture->load( filename, 0 ) == false )
    throw std::runtime_error ( "Texture could not be load!" );
}

void GLTexture::init ( GLenum gl_textNumber )
{
  assert ( texture != nullptr );
  
  gltexture_id = gl_textNumber;
  
  if ( texture->getColorType() == 2 )
  {
    glActiveTexture ( gl_textNumber );
    glBindTexture ( GL_TEXTURE_2D, gl_texture );
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGB, texture->getWidth(), texture->getHeight(),
		   0, GL_BGR, GL_UNSIGNED_BYTE, texture->accessPixels() );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
}


void GLTexture::release_host_memory()
{
  delete texture;
  texture = 0;
}


GLTexture::~GLTexture()
{
  glDeleteTextures ( 1, &gl_texture );
}
