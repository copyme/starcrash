/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#include <algorithm>
#include "texturedmesh.h"

static void ActiveTexture ( GLTexture * texture ) 
{
    texture->bind();
}

static void Paint ( AbstractMesh * mesh ) 
{
    mesh->render();
}

static void remove_texture ( GLTexture * texture ) 
{
    delete texture;
}

static void remove_mesh ( AbstractMesh * mesh ) 
{
    delete mesh;
}

void TexturedMesh::add_geometry ( std::vector< tinyobj::shape_t >& shape )
{
  for (uint i = 0; i < shape.size(); i++ )
  {
    STP3D::IndexedMesh * t_glMesh = new STP3D::IndexedMesh( shape[i].mesh.indices.size()/3, shape[i].mesh.positions.size()/3 );
    t_glMesh->addIndexBuffer( const_cast < uint * > ( &(shape[i].mesh.indices[0]) ), false);
    t_glMesh->addOneBuffer(0, 3, const_cast < float * > ( &shape[i].mesh.positions[0] ), std::string("position"), false);
    t_glMesh->addOneBuffer(1, 3, const_cast < float * > ( &shape[i].mesh.normals[0] ), std::string("normal"), false);
    t_glMesh->addOneBuffer(2, 2, const_cast < float * > ( &shape[i].mesh.texcoords[0] ), std::string("uv"), false);
    t_glMesh->createVAO();
    glMesh.push_back(t_glMesh);
  }
}


void TexturedMesh::add_texture ( const char * file, GLenum texture_id )
{
    GLTexture * texture = new GLTexture();
    texture->load(file);
    texture->init ( texture_id );
    texture->release_host_memory();
    textures.push_back(texture);
}

void TexturedMesh::draw()
{
  std::for_each ( textures.begin(), textures.end(), ActiveTexture );
  std::for_each ( glMesh.begin(), glMesh.end(), Paint);
}


TexturedMesh::~TexturedMesh()
{
  std::for_each ( textures.begin(), textures.end(), remove_texture );
  std::for_each ( glMesh.begin(), glMesh.end(), remove_mesh );
}
