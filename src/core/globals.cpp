#include "globals.h"
#include <glm/gtc/matrix_transform.hpp>

Camera camera;
glm::mat4 projection;
glm::mat4 worldToView;
glm::mat4 screenToWorld;

glm::mat4 MAT4F_M1_P1_TO_P0_P1(
  0.5f, 0.f, 0.f, 0.f,
  0.f, 0.5f, 0.f, 0.f,
  0.f, 0.f, 0.5f, 0.f,
  0.5f, 0.5f, 0.5f, 1.f
);
glm::mat4 ligthToShadowMap = glm::perspective(60.f, 1.f, 1.f, 1000.f);