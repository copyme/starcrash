/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#include "unitplane.h"

const int UnitPlane:: quad_triangleCount = 2;

UnitPlane::UnitPlane()
{
  int   quad_triangleList[] = {0, 1, 2, 2, 1, 3}; 
  float quad_vertices[] =  {-1.0, -1.0, 1.0, -1.0, -1.0, 1.0, 1.0, 1.0};
  

  glGenVertexArrays(1, &vao);
  glGenBuffers(2, vbo);
  
  // Quad
  glBindVertexArray(vao);
  // Bind indices and upload data
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[0]);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(quad_triangleList), quad_triangleList, GL_STATIC_DRAW);
  // Bind vertices and upload data
  glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GL_FLOAT)*2, (void*)0);
  glBufferData(GL_ARRAY_BUFFER, sizeof(quad_vertices), quad_vertices, GL_STATIC_DRAW);
}



void UnitPlane::draw()
{
  glBindVertexArray(vao);
  glDrawElements(GL_TRIANGLES, quad_triangleCount * 3, GL_UNSIGNED_INT, (void*)0);
}
