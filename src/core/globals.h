#ifndef CORE_GLOBALS_H
#define CORE_GLOBALS_H

#include <glm/glm.hpp>
#include <memory>
#include "camera.h"
#define OPENGL_MAJOR_VERSION 3
#define OPENGL_MINOR_VERSION 3

extern Camera camera;
extern glm::mat4 projection;
extern glm::mat4 worldToView;
extern glm::mat4 screenToWorld;
extern glm::mat4 MAT4F_M1_P1_TO_P0_P1;
extern glm::mat4 ligthToShadowMap;

// Standard types.

using uint = unsigned int;

// Geometric types.

struct AABB  // Axis-aligned bounding box.
{ 
    glm::vec3 min;
    glm::vec3 max;
};

class AbstractMesh
{
public:
  virtual void render () = 0;
};


#endif // !CORE_GLOBALS_H
