#include <core/environment.h>
#include <tiny_obj_loader.h>
#include <FreeImagePlus.h>
#include <glutils.h>
#include <iostream>
#include <stdexcept>
#include <core/globals.h>
#include <glm/gtc/matrix_transform.hpp>

using namespace std;

const char* vertex_shader =  
"#version 330 core \n\
\
layout( location = 0 ) in vec3 in_pos;\n \
layout( location = 1 ) in vec2 in_uv;\n \
\n\
out vec2 UV;\n \
uniform mat4 MVP;\n \
\n\
void main ( void )\n \
{ \n\
    gl_Position =  MVP * vec4 ( in_pos, 1.f );\n \
    UV = in_uv;\n \
}";

const char* fragment_shader = 
"#version 330 core\n \
 \n\
in vec2 UV;\n \
out vec3 color;\n \
\n\
uniform sampler2D sampler;\n \
 \
void main ( void )\n \
{\n\
      color = texture( sampler, UV ).rgb;\n \
}";

Environment::Environment ( const char*     texture_path,
                                 float     size,
                                 glm::vec3 offset ) : sphere_vao { },
                                                      size { size },
                                                      texture_path { texture_path },
                                                      offset ( offset )
{
}

void Environment::render ( const glm::mat4& proj, const glm::mat4& view )
{
    if ( sphere_vao == 0 )
    {
        loadGL ( );
        loadTex ( );
    }

    glm::mat4 MVP { };
    //MVP *= size;
    glm::mat4 scale { size, .0f, .0f, .0f,
                      .0f, size, .0f, .0f,
                      .0f, .0f, size, .0f,
                      .0f, .0f, .0f, 1.f,};
    glm::mat4 translation = glm::translate ( glm::mat4 { }, offset );
    
    MVP = proj * view * translation * scale;

    shader.use ( );
    shader.setUniform ( "MVP", MVP );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    glBindVertexArray ( sphere_vao );
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, sphere_vbo[ 2 ] );

    glActiveTexture ( GL_TEXTURE0 );
    glBindTexture  ( GL_TEXTURE_2D, envmap_texture );
    shader.setUniform ( "sampler", 0 );
    glDrawElements ( GL_TRIANGLES, 
                     num_indices,
                     GL_UNSIGNED_INT,
                     0 );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

void Environment::loadGL ( void )
{
    if ( !shader.compileShaderFromString ( vertex_shader, GLSLShader::VERTEX ) )
    {
        cout << shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders failed!" );
    }

    if ( !shader.compileShaderFromString ( fragment_shader, GLSLShader::FRAGMENT ) )
    {
        cout << shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders failed!" );
    }

    if ( !shader.link ( ) )
    {
        throw std::runtime_error ( "Linking of shaders failed: " + shader.log() );
    }

    glGenVertexArrays ( 1, &sphere_vao );
    glBindVertexArray ( sphere_vao );
    glEnableVertexAttribArray ( 0 );
    glEnableVertexAttribArray ( 1 );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    vector< tinyobj::shape_t > shape;
    tinyobj::LoadObj ( shape,
                       "models/envSphere.obj",
                       "models/");
    
    glGenBuffers ( 3, sphere_vbo );
    // Bind Position attribute.
    glBindBuffer ( GL_ARRAY_BUFFER, sphere_vbo[ 0 ] );
    glVertexAttribPointer ( 0,
                            3,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            nullptr );
    glBufferData ( GL_ARRAY_BUFFER, 
                   sizeof ( float ) * shape.at ( 0 ).mesh.positions.size ( ), 
                   &shape.at ( 0 ).mesh.positions[ 0 ], 
                   GL_STATIC_DRAW );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    // Bind UV attribute.
    glBindBuffer ( GL_ARRAY_BUFFER, sphere_vbo[ 1 ] );
    glVertexAttribPointer ( 1,
                            2,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            0 );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    glBufferData ( GL_ARRAY_BUFFER, 
                   sizeof ( float ) * shape.at ( 0 ).mesh.texcoords.size ( ), 
                   &shape.at ( 0 ).mesh.texcoords[ 0 ], 
                   GL_STATIC_DRAW );

    // Load indices.
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, sphere_vbo[ 2 ] );
    glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 
                   sizeof ( uint ) * shape.at ( 0 ).mesh.indices.size ( ), 
                   &shape.at ( 0 ).mesh.indices[ 0 ], 
                   GL_STATIC_DRAW ); 
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    glBindVertexArray ( 0 );
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindBuffer ( GL_ARRAY_BUFFER, 0 );

    num_indices = shape.at ( 0 ).mesh.indices.size ( );
}

void Environment::loadTex ( void )
{
    fipImage image;
    if ( image.load ( texture_path.c_str ( ), 0 ) == false )
    {
        throw std::runtime_error ( "Image could not be load" );
    }

    glGenTextures ( 1, &envmap_texture);
    glBindTexture (GL_TEXTURE_2D, envmap_texture);
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    // Give the image to OpenGL
    glTexImage2D ( GL_TEXTURE_2D,
                   0,
                   GL_RGB,
                   image.getWidth ( ),
                   image.getHeight ( ),
                   0, 
                   GL_BGR, 
                   GL_UNSIGNED_BYTE, 
                   image.accessPixels ( ) );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
}