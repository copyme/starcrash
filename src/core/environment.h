#ifndef CORE_ENVIRONMENT_H
#define CORE_ENVIRONMENT_H

#include <string>
#include <GL/glew.h>
#include <glslprogram.h>
#include <glm/glm.hpp>
#include <core/globals.h>
#include <glm/glm.hpp>

class Environment
{
public:
    Environment ( void ) = delete;
    Environment ( const char*     texture_path, 
                        float     size = 50000.f,
                        glm::vec3 offset = { .0f, .0f, .0f } );

    void set_size ( float new_size ) { size = new_size; }
    float get_size ( void ) { return size; }
    void render ( const glm::mat4& proj, const glm::mat4& view );
private:
    float size;
    glm::vec3 offset;

    GLSLProgram shader;
    GLuint sphere_vao;
    GLuint sphere_vbo[ 3 ]; // Vertices + uv + indices.

    GLuint envmap_texture;
    std::string texture_path;

    uint num_indices;

    void loadGL ( void );
    void loadTex ( void );
};
#endif // !CORE_ENVIRONMENT_H
