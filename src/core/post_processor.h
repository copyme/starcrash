#ifndef CORE_POST_PROCESSOR_H
#define CORE_POST_PROCESSOR_H

#include <GL/glew.h>
#include <core/unitplane.h>
#include <core/globals.h>
#include <glm/glm.hpp>
#include "environment.h"


class Post_processor
{
public:
    Post_processor ( void );
    Post_processor ( int width, int height );

    void set_beauty_pass ( GLuint beauty_pass_id );
    void set_z_pass      ( GLuint z_pass_id      );
    void set_bg_pass     ( GLuint bg_pass_id     );

    void add_environment ( void );
	void add_planetbackground(Environment* planet);
    void add_light_shaft ( glm::vec3 light_pos );

    GLuint get_output_texture ( void ) const { return out_pass; }

    void target_size ( int new_width, int new_height );
private:
    static UnitPlane plane;

    int width;
    int height;

    GLuint fbo;
    GLuint beauty_pass;
    GLuint z_pass;
    GLuint background_pass;

    GLuint occlusion_pass;
    GLuint out_pass;
};
#endif // !CORE_POST_PROCESSOR_H
