#include <core/PCloud.h>

#include <stdexcept>
#include <cstddef>
#include <random>
#include <iostream>

#include <glutils.h>
#include <glm/gtx/rotate_vector.hpp>

using namespace std;

static default_random_engine dre { 1505 };

//////////////////////////////////////////////
//              PCloud definitions.
//////////////////////////////////////////////

PCloud::PCloud ( void ) : active_group { }, 
                          current_group_id { }
{
    groups.reserve ( 20 );
    timestep = 0.01f;
}


uint PCloud::add_group ( void )
{
    active_group = current_group_id;

    PGroup grp { current_group_id };

    uniform_real_distribution< float > dr;
    grp.color.r = dr ( dre );
    grp.color.g = dr ( dre );
    grp.color.b = dr ( dre );
    grp.color.a = 1.f;

    groups.push_back ( grp );

    return current_group_id++;
}


void PCloud::set_active_group ( uint group_id )
{
    active_group = group_id;
}


void PCloud::set_group_shape ( const char* file, const char* mtl_path )
{
    vector< tinyobj::shape_t > shape;
    tinyobj::LoadObj ( shape, file, mtl_path );

    groups.at ( active_group ).load_shape ( shape.at ( 0 ).mesh );
}


void PCloud::add_particle ( const Particle& particle )
{
    groups.at ( active_group ).particles.push_back ( particle );
}


void PCloud::generate_particles ( uint               num_particles, 
                                  const AABB&        bounding_volume,
                                  const Birth_param& birth_param )
{
    for ( uint i = 0; i < num_particles; i++ )
    {
        uniform_real_distribution< float > dr_x { bounding_volume.min.x, bounding_volume.max.x };
        uniform_real_distribution< float > dr_y { bounding_volume.min.y, bounding_volume.max.y };
        uniform_real_distribution< float > dr_z { bounding_volume.min.z, bounding_volume.max.z };

        Particle p;
        p.position.x = dr_x ( dre );
        p.position.y = dr_y ( dre );
        p.position.z = dr_z ( dre );

        uniform_real_distribution< float > dir_x { - birth_param.direction_variation.x, birth_param.direction_variation.x };
        uniform_real_distribution< float > dir_y { - birth_param.direction_variation.y, birth_param.direction_variation.y };
        uniform_real_distribution< float > dir_z { - birth_param.direction_variation.z, birth_param.direction_variation.z };

        p.direction = glm::normalize ( birth_param.direction_of_travel );
        p.direction = glm::rotateX ( p.direction, dir_x ( dre ) );
        p.direction = glm::rotateY ( p.direction, dir_y ( dre ) );
        p.direction = glm::rotateZ ( p.direction, dir_z ( dre ) );

        uniform_real_distribution< float > speed { birth_param.speed - birth_param.speed_variation, birth_param.speed + birth_param.speed_variation };
        p.speed = speed ( dre );

        uniform_real_distribution< float > size { birth_param.size - birth_param.size_variation, birth_param.size + birth_param.size_variation };
        p.size = size ( dre );

        groups.at ( active_group ).particles.push_back ( p );

        uniform_real_distribution< float > spin { glm::radians ( birth_param.spin_amount - birth_param.spin_variation ), glm::radians ( birth_param.spin_amount + birth_param.spin_variation ) };
        uniform_real_distribution< float > spin_x { -birth_param.spin_axis_variation.x, birth_param.spin_axis_variation.x };
        uniform_real_distribution< float > spin_y { -birth_param.spin_axis_variation.y, birth_param.spin_axis_variation.y  };
        uniform_real_distribution< float > spin_z { -birth_param.spin_axis_variation.z, birth_param.spin_axis_variation.z  };

        
        glm::fquat q;
        q = glm::rotate ( q, 
                          spin ( dre ), 
                          glm::normalize ( glm::vec3 { spin_x ( dre ), spin_y ( dre ), spin_z ( dre ) } ) );
        groups.at ( active_group ).orientations.push_back ( glm::normalize ( q ) );
        groups.at ( active_group ).rotation_amount.push_back ( glm::normalize ( q ) );
    }
}


void PCloud::updateGL ( void )
{
    groups.at ( active_group ).uploadGL ( );
}


void PCloud::render ( )
{
    for ( auto& g : groups )
    {
        g.render ( projection, worldToView, camera.eye, timestep );
    }
}

//////////////////////////////////////////////
//              PGroup definitions.
//////////////////////////////////////////////
PCloud::PGroup::PGroup ( uint gid ) : group_id { gid },
                                      vao      {     },
                                      mesh_vao {     }
{
    if ( !particle_shader.compileShaderFromFile ( "shaders/basic.vert", GLSLShader::VERTEX ) )
    {
        cout << particle_shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }
    
    if ( !particle_shader.compileShaderFromFile ( "shaders/gbuffer.frag", GLSLShader::FRAGMENT ) )
    {
        cout << particle_shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }
    particle_shader.link ( );
    if ( particle_shader.isLinked ( ) == false )
    {
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }
    
    if ( !instance_shader.compileShaderFromFile ( "shaders/instanceBasic.vert", GLSLShader::VERTEX ) )
    {
        cout << instance_shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }
    
    if ( !instance_shader.compileShaderFromFile ( "shaders/gbuffer.frag", GLSLShader::FRAGMENT ) )
    {
        cout << instance_shader.log ( );
        cin.get ( );
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }

    instance_shader.link ( );
    if ( instance_shader.isLinked ( ) == false )
    {
        throw std::runtime_error ( "Compilation of shaders faild!" );
    }
}


void PCloud::PGroup::uploadGL ( void )
{

    if ( vao == 0 ) // Means nothing has been initialized.
    {
        glGetError ( ); // Clear error.
        glGenVertexArrays ( 1, &vao );
        glBindVertexArray ( vao );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glEnableVertexAttribArray ( Particle::POS_VERT_ATTR );
        glEnableVertexAttribArray ( Particle::DIR_VERT_ATTR );
        glEnableVertexAttribArray ( Particle::SPE_VERT_ATTR );
        glEnableVertexAttribArray ( Particle::SIZ_VERT_ATTR );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );


        glGenBuffers ( 2, vbo );
        glBindBuffer ( GL_ARRAY_BUFFER, vbo[ 0 ] );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glVertexAttribPointer ( Particle::POS_VERT_ATTR,
                                3,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                nullptr );
        glVertexAttribPointer ( Particle::DIR_VERT_ATTR,
                                3,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, direction ) ) );
        glVertexAttribPointer ( Particle::SPE_VERT_ATTR,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, speed ) ) );
        glVertexAttribPointer ( Particle::SIZ_VERT_ATTR,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, size ) ) );

        // Orientation buffer
        glGenBuffers ( 1, &orientation_vbo );
        glEnableVertexAttribArray ( 4 );
        glBindBuffer ( GL_ARRAY_BUFFER, orientation_vbo );
        glVertexAttribPointer ( 4,
                                4,
                                GL_FLOAT,
                                GL_FALSE,
                                0,
                                nullptr);
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        // Setup transform feedback with shader.
        if ( !sim_shader.compileShaderFromFile ( "shaders/update_particles.vert", GLSLShader::VERTEX ) )
        {
            cout << sim_shader.log ( );
        }
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        const GLchar* feedback_varying[ ] = { "new_position", 
                                              "new_direction",
                                              "new_speed",
                                              "new_size" };
        glTransformFeedbackVaryings ( sim_shader.getHandle ( ), 
                                      4, 
                                      feedback_varying,
                                      GL_INTERLEAVED_ATTRIBS );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        sim_shader.link ( );
        if ( particle_shader.isLinked ( ) == false )
        {
            throw std::runtime_error ( "Compilation of shaders faild!" );
        }

        

        glBindBuffer ( GL_ARRAY_BUFFER, 0 );
        glBindVertexArray ( 0 );
    }

    if ( particles.size ( ) > 0 )
    {
        glBindBuffer ( GL_ARRAY_BUFFER, vbo[ 0 ] );

        glBufferData ( GL_ARRAY_BUFFER, 
                       sizeof ( Particle ) * particles.size ( ),
                       &particles[ 0 ],
                       GL_STATIC_DRAW );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindBuffer ( GL_ARRAY_BUFFER, vbo[ 1 ] );
        glBufferData ( GL_ARRAY_BUFFER, 
                       sizeof ( Particle ) * particles.size ( ), 
                       nullptr, 
                       GL_STATIC_READ);
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        // Orientation.
        glBindBuffer ( GL_ARRAY_BUFFER, orientation_vbo );
        glBufferData ( GL_ARRAY_BUFFER, 
                       sizeof ( glm::fquat ) * particles.size ( ), 
                       &orientations[ 0 ], 
                       GL_STATIC_READ );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindBuffer ( GL_ARRAY_BUFFER, 0 );
    }
}


void PCloud::PGroup::load_shape ( const tinyobj::mesh_t& shape )
{
    glGenVertexArrays ( 1, &mesh_vao );
    glBindVertexArray ( mesh_vao );
    glEnableVertexAttribArray ( 0 );
    glEnableVertexAttribArray ( 1 );
    glEnableVertexAttribArray ( 2 ); // Instance position.
    glEnableVertexAttribArray ( 3 ); // Particle size.
    glEnableVertexAttribArray ( 4 ); // Particle orientation.
    glEnableVertexAttribArray ( 5 ); // UVs.
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    glGenBuffers ( 4, mesh_vbo );

    // Positions
    glBindBuffer ( GL_ARRAY_BUFFER, mesh_vbo[ 0 ] );
    glVertexAttribPointer ( 0,
                            3,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            nullptr );
    glBufferData ( GL_ARRAY_BUFFER, 
                   sizeof ( float ) * shape.positions.size ( ), 
                   &shape.positions[ 0 ], 
                   GL_STATIC_DRAW );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    // Normals
    glBindBuffer ( GL_ARRAY_BUFFER, mesh_vbo[ 1 ] );
    glVertexAttribPointer ( 1,
                            3,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            0 );
    glBufferData ( GL_ARRAY_BUFFER, 
                   sizeof ( float ) * shape.normals.size ( ), 
                   &shape.normals[ 0 ], 
                   GL_STATIC_DRAW ); 
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    // Indices
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, mesh_vbo[ 2 ] );
    glBufferData ( GL_ELEMENT_ARRAY_BUFFER, 
                   sizeof ( uint ) * shape.indices.size ( ), 
                   &shape.indices[ 0 ], 
                   GL_STATIC_DRAW ); 
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    
    
    // UVs
    glBindBuffer ( GL_ARRAY_BUFFER, mesh_vbo[ 3 ] );
    glVertexAttribPointer ( 5,
                            2,
                            GL_FLOAT,
                            GL_FALSE,
                            0,
                            0 );
    glBufferData ( GL_ARRAY_BUFFER, 
                   sizeof ( float ) * shape.texcoords.size ( ), 
                   &shape.texcoords[ 0 ], 
                   GL_STATIC_DRAW ); 
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindBuffer ( GL_ARRAY_BUFFER, 0 );
    glBindVertexArray ( 0 );

    num_vertices = shape.positions.size ( ) / 3;
    num_indices  = shape.indices.size ( ); 
}


  void PCloud::PGroup::step_sim ( float timestep )
  {
      if ( vao != 0 )
    {
        sim_shader.use ( );
        sim_shader.setUniform ( "timestep", timestep );

        glBindVertexArray ( vao );
        glDisableVertexAttribArray ( 4 );
        glBindBuffer ( GL_ARRAY_BUFFER, vbo[ 0 ] );
        glVertexAttribPointer ( Particle::POS_VERT_ATTR,
                                3,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                nullptr );
        glVertexAttribPointer ( Particle::DIR_VERT_ATTR,
                                3,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, direction ) ) );
        glVertexAttribPointer ( Particle::SPE_VERT_ATTR,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, speed ) ) );
        glVertexAttribPointer ( Particle::SPE_VERT_ATTR,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, speed ) ) );
        glVertexAttribPointer ( Particle::SIZ_VERT_ATTR,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, size ) ) );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindBufferBase ( GL_TRANSFORM_FEEDBACK_BUFFER, 
                           0, 
                           vbo[ 1 ] );
        glBindBufferBase ( GL_TRANSFORM_FEEDBACK_BUFFER, 
                           1, 
                           vbo[ 1 ] );
        glBindBufferBase ( GL_TRANSFORM_FEEDBACK_BUFFER, 
                           2, 
                           vbo[ 1 ] );
        glBindBufferBase ( GL_TRANSFORM_FEEDBACK_BUFFER, 
                           3, 
                           vbo[ 1 ] );

        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glEnable ( GL_RASTERIZER_DISCARD );
        glBeginTransformFeedback ( GL_POINTS );
        glDrawArrays ( GL_POINTS, 0, particles.size ( ) );
        glEndTransformFeedback();
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        swap ( vbo[ 0 ], vbo[ 1 ] );
        

        glDisable ( GL_RASTERIZER_DISCARD );

        for ( uint i = 0; i < orientations.size ( ); i++ )
        {
            glm::fquat q = rotation_amount.at ( i ) * orientations.at ( i );
            orientations.at ( i ) = glm::normalize ( q );
        }

        glBindBuffer ( GL_ARRAY_BUFFER, orientation_vbo );
        glBufferData ( GL_ARRAY_BUFFER, 
                       sizeof ( glm::fquat ) * particles.size ( ), 
                       &orientations[ 0 ], 
                       GL_STATIC_READ );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    }
  }


void PCloud::PGroup::render ( const glm::mat4& proj,
                              const glm::mat4& world,
                              const glm::vec3& cam_eye,
                              float timestep )
{
    step_sim ( timestep );

    if ( mesh_vao == 0 )
    {
        particle_shader.use ( );
        particle_shader.setUniform ( "Projection", proj );
        particle_shader.setUniform ( "View", world );

        glBindVertexArray ( vao );
        glEnableVertexAttribArray ( 4 );
        glPointSize ( 4.f );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glDrawArrays ( GL_POINTS, 0, particles.size ( ) );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    }
    else
    {
        instance_shader.use ( );
        particle_shader.setUniform ( "Projection", proj );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        particle_shader.setUniform ( "View", world );
        
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        
        glBindVertexArray ( mesh_vao );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glBindBuffer ( GL_ARRAY_BUFFER, vbo[ 0 ] );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glVertexAttribPointer ( 2,
                                3,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                nullptr );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        glVertexAttribPointer ( 3,
                                1,
                                GL_FLOAT,
                                GL_FALSE,
                                sizeof ( Particle ),
                                reinterpret_cast<void*>( offsetof( Particle, size ) ) );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glEnableVertexAttribArray ( 4 );
        glBindBuffer ( GL_ARRAY_BUFFER, orientation_vbo );
        
        glVertexAttribPointer ( 4,
                                4,
                                GL_FLOAT,
                                GL_FALSE,
                                0,
                                nullptr);
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, mesh_vbo[ 2 ] );
        glVertexAttribDivisor ( 2, 1 );
        glVertexAttribDivisor ( 3, 1 );
        glVertexAttribDivisor ( 4, 1 );
        glDrawElementsInstanced ( GL_TRIANGLES,
                                  num_indices,
                                  GL_UNSIGNED_INT,
                                  0, 
                                  particles.size ( ) );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    }
    glBindBuffer ( GL_ELEMENT_ARRAY_BUFFER, 0 );
    glBindVertexArray ( 0 );
    glUseProgram(0);
}


PCloud::PGroup::~PGroup ( void )
{
    /*if ( vao != 0 && vbo != 0) 
    {
        glDeleteBuffers ( 1, &vbo );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindVertexArray ( vao );
        glDisableVertexAttribArray ( Particle::POS_VERT_ATTR );
        glDisableVertexAttribArray ( Particle::DIR_VERT_ATTR );
        glDisableVertexAttribArray ( Particle::SPE_VERT_ATTR );
        glDeleteVertexArrays ( 1, &vao );
        GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

        glBindBuffer ( GL_ARRAY_BUFFER, 0 );
        glBindVertexArray ( 0 );
    }*/
}