#ifndef CORE_PCLOUD_H
#define CORE_PCLOUD_H

#include <vector>
#include <GL/glew.h>
#include <core/globals.h>
#include <glslprogram.h>
#include <tiny_obj_loader.h>
#include <glm/gtc/quaternion.hpp>

struct Particle
{
    glm::vec3 position;
    glm::vec3 direction;
    float     speed;
    float     size;
 
    enum { POS_VERT_ATTR, 
           DIR_VERT_ATTR,
           SPE_VERT_ATTR,
           SIZ_VERT_ATTR };
    Particle ( void ) : speed { }, size { 1.f }
    { }
};


struct Birth_param
{
    glm::vec3  direction_of_travel;
    glm::vec3  direction_variation; // In degrees.
    float      speed;
    float      speed_variation;
    float      size;
    float      size_variation;
    float      spin_amount;    // In degrees.
    float      spin_variation; // In degrees.
    glm::vec3  spin_axis_variation; // In degrees.
    
    Birth_param ( void ) : direction_of_travel ( glm::vec3 ( 1.f, .0f, .0f ) ),
                           direction_variation ( glm::vec3 ( .0f ) ),
                           speed           { },
                           speed_variation { },
                           size            { },
                           size_variation  { },
                           spin_amount     { },
                           spin_variation  { },
                           spin_axis_variation { glm::vec3 { 1.f, .0f, .0f } }
    {
    }
};

class PCloud : public AbstractMesh
{
private:
  float timestep;
public:
    PCloud ( void );

    PCloud            ( const PCloud& ) = delete;
    PCloud& operator= ( const PCloud& ) = delete;

    uint add_group ( void );  // Returns id and set group id active.
    void set_active_group ( uint group_id );
    void set_group_shape ( const char* file, const char* mtl_path );  // Set the shape for the active group.

    void add_particle ( const Particle& particle );
    void generate_particles (       uint         num_particles, 
                              const AABB&        bounding_volume,
                              const Birth_param& birth_param );
    void updateGL ( void );
    
    void render ( );

private:
    struct PGroup;
    std::vector<PGroup> groups;
    uint                active_group;
    uint                current_group_id;

    struct PGroup
    {
        // Group related data.
        uint                    group_id;
        glm::vec4               color;
        std::vector< Particle > particles;
        std::vector< glm::fquat > orientations; // Because transform feedback fail with 4 outputs here.
        std::vector< glm::fquat > rotation_amount;

        // OpenGL related data.
        GLuint vao;
        GLuint vbo[ 2 ];
        GLuint orientation_vbo;
        
        GLuint mesh_vao;
        GLuint mesh_vbo[ 4 ]; // Position, normals, indices.
        uint num_vertices;
        uint num_indices;

        GLSLProgram particle_shader; 
        GLSLProgram sim_shader;
        GLSLProgram instance_shader;

        PGroup  ( void     ) = delete;
        PGroup  ( uint gid );
        ~PGroup ( void     );

        void uploadGL ( void );
        void load_shape ( const tinyobj::mesh_t& shape );
        void step_sim ( float timestep );
        void render ( const glm::mat4& proj,
                      const glm::mat4& world,
                      const glm::vec3& cam_eye,
                      float timestep );
    };

};

#endif // !CORE_PCLOUD_H
