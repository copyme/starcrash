#include <core/post_processor.h>
#include <glslprogram.h>
#include <glutils.h>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>


using namespace std;



Post_processor::Post_processor ( void ) : Post_processor { 800, 600 }
{ 
}

Post_processor::Post_processor ( int width, int height ) : fbo { 0 },
                                                           beauty_pass     { 0 },
                                                           z_pass          { 0 },
                                                           out_pass        { 0 },
                                                           background_pass { 0 },
                                                           occlusion_pass  { 0 },
                                                           width  { width }, 
                                                           height { height }
                                                             
{ 
    int err;
    glGenFramebuffers ( 1, &fbo );

    err = GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    if ( err == 1 )
    {
        throw std::runtime_error ( "Failed to create FBO in post processor" );
    }

    glGenTextures ( 1, &out_pass );
    // Create color texture
    glBindTexture ( GL_TEXTURE_2D, out_pass );
    glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGBA16F, width, height, 0, GL_RGBA, GL_FLOAT, 0 );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameterf ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
    glTexParameterf ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

    err = GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    if ( err == 1 )
    {
        throw std::runtime_error ( "Failed to create out pass in post processor" );
    }
}


void Post_processor::set_beauty_pass ( GLuint beauty_pass_id )
{
    beauty_pass = beauty_pass_id;
}


void Post_processor::set_z_pass ( GLuint z_pass_id )
{
    z_pass = z_pass_id;
}


void Post_processor::set_bg_pass ( GLuint bg_pass_id )
{
    background_pass = bg_pass_id;
}


void Post_processor::add_environment ( )
{
    static UnitPlane render_plane;
    static GLSLProgram shader = []() 
    {
        GLSLProgram shader; 
        bool status;
        status =  shader.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
        status &= shader.compileShaderFromFile ( "shaders/gbuffer_debug.frag" , GLSLShader::FRAGMENT );

        if ( !status )
        {
            cout << shader.log ( );
            throw std::runtime_error ( "Compilation of shaders faild: " + shader.log ( ) );
        }
  
        shader.link ( );
        if ( shader.isLinked ( ) == false )
        {
            throw std::runtime_error ( "Linking of shaders faild: " + shader.log ( ) );
        }

        return shader;
    } ( );

    if ( background_pass == 0 || beauty_pass == 0 )
    {
        cerr << "Beauty pass or background pass not set for background effect.\n";
        return;
    }

    shader.use();
    shader.setUniform("Texture1", 0);
    shader.setUniform("Env", 1);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, beauty_pass );
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, background_pass );
    
    glBindFramebuffer ( GL_FRAMEBUFFER, fbo );
    glFramebufferTexture2D ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, out_pass, 0 );
    GLenum draw_buffers[ 1 ] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers ( 1, draw_buffers );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    glDisable ( GL_DEPTH_TEST );
    render_plane.draw();
    glEnable ( GL_DEPTH_TEST );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );

    glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
}

void Post_processor::add_planetbackground(Environment* planet)
{
	glBindFramebuffer(GL_FRAMEBUFFER, fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, background_pass, 0);
	GLenum draw_buffers[1] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, draw_buffers);
	GLUtils::checkForOpenGLError(__FILE__, __LINE__);

	glDisable(GL_DEPTH_TEST);
    glFrontFace ( GL_CW );
    float old_size = planet->get_size ( );
    planet->render(glm::perspective(45.0f, (float)width / (float)height, .1f, 100000.f), glm::lookAt(camera.eye, camera.o, camera.up));
    glEnable ( GL_BLEND );
    planet->set_size ( old_size + 100.f );
	planet->render(glm::perspective(45.0f, (float)width / (float)height, .1f, 100000.f), glm::lookAt(camera.eye, camera.o, camera.up));
    planet->set_size ( old_size  );
    glFrontFace ( GL_CCW );
    glDisable ( GL_BLEND );
	glEnable(GL_DEPTH_TEST);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Post_processor::add_light_shaft ( glm::vec3 light_pos )
{
    static UnitPlane render_plane;
    static GLSLProgram shader = []() 
    {
        GLSLProgram shader; 
        bool status;
        status =  shader.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
        status &= shader.compileShaderFromFile ( "shaders/light_shaft.frag" , GLSLShader::FRAGMENT );

        if ( !status )
        {
            cout << shader.log ( );
            throw std::runtime_error ( "Compilation of shaders faild: " + shader.log ( ) );
        }
  
        shader.link ( );
        if ( shader.isLinked ( ) == false )
        {
            throw std::runtime_error ( "Linking of shaders faild: " + shader.log ( ) );
        }

        return shader;
    } ( );

    static GLSLProgram light_shader = []() 
    {
        GLSLProgram shader; 
        bool status;
        status =  shader.compileShaderFromFile ( "shaders/circle_draw.vert" , GLSLShader::VERTEX );
        status &= shader.compileShaderFromFile ( "shaders/circle_draw.frag" , GLSLShader::FRAGMENT );

        if ( !status )
        {
            cout << shader.log ( );
            throw std::runtime_error ( "Compilation of shaders faild: " + shader.log ( ) );
        }
  
        shader.link ( );
        if ( shader.isLinked ( ) == false )
        {
            throw std::runtime_error ( "Linking of shaders faild: " + shader.log ( ) );
        }

        return shader;
    } ( );

    static GLSLProgram blur_shader = []() 
    {
        GLSLProgram shader; 
        bool status;
        status =  shader.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
        status &= shader.compileShaderFromFile ( "shaders/radial_blur.frag" , GLSLShader::FRAGMENT );

        if ( !status )
        {
            cout << shader.log ( );
            throw std::runtime_error ( "Compilation of shaders faild: " + shader.log ( ) );
        }
  
        shader.link ( );
        if ( shader.isLinked ( ) == false )
        {
            throw std::runtime_error ( "Linking of shaders faild: " + shader.log ( ) );
        }

        return shader;
    } ( );

    if ( beauty_pass == 0 || z_pass == 0 )
    {
        cerr << "Beauty pass or Z pass not set for light shaft effect.\n";
        return;
    }
    
    if ( occlusion_pass == 0 )
    {
        glGenTextures ( 1, &occlusion_pass );
        // Create color texture
        glBindTexture ( GL_TEXTURE_2D, occlusion_pass );
        glTexImage2D ( GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0 );
        glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri ( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameterf ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameterf ( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );

        int err = GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
        if ( err == 1 )
        {
            throw std::runtime_error ( "Failed to create out pass in post processor" );
        }
    }

        
    
    glBindFramebuffer ( GL_FRAMEBUFFER, fbo );
    GLenum draw_buffers[ 1 ] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers ( 1, draw_buffers );
    
    // First render white circle for light.
    
    glm::mat4 rotation_matrix = glm::rotate ( glm::mat4 { }, 
                                              -65.f, 
                                              glm::vec3 {.0f, 1.f, .0f } );
    rotation_matrix = glm::rotate ( rotation_matrix, 
                                    35.f, 
                                    glm::vec3 { 1.f, .0f, .0f } );
    glm::mat4 model_matrix = rotation_matrix *
                             glm::translate ( glm::mat4 { }, glm::vec3 { .0f, .0f, -10000.f } ) * 
                             glm::scale ( glm::mat4 { }, glm::vec3 { 10000.f } );
    glm::mat4 mvp { };
    projection = glm::perspective ( 45.0f, ( float ) width / ( float ) height, .1f, 100000.f );
    worldToView = glm::lookAt( camera.eye, camera.o, camera.up);
    mvp = projection * ( worldToView * model_matrix );

    glFramebufferTexture2D ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, out_pass, 0 );
    glClearColor ( .0f, .0f, .0f, 1.f );
    glClear ( GL_COLOR_BUFFER_BIT );
    light_shader.use ( );
    light_shader.setUniform ( "MVP", mvp );

    render_plane.draw ( ); 
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
 
       
    // Render occluded object on top.
    glFramebufferTexture2D ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, occlusion_pass, 0 );
    glActiveTexture ( GL_TEXTURE0 );
    glBindTexture ( GL_TEXTURE_2D, out_pass );
    glActiveTexture ( GL_TEXTURE1 );
    glBindTexture ( GL_TEXTURE_2D, z_pass );
    GLUtils::checkForOpenGLError ( __FILE__, __LINE__ );
    
    shader.use ( );
    shader.setUniform ( "z_depth", 1 );
    shader.setUniform ( "lights", 0 );
    render_plane.draw ( );

    // Do radial effect
    GLint viewport_matrix[ 4 ];
    glGetIntegerv( GL_VIEWPORT, viewport_matrix );

    glm::vec4 viewport { viewport_matrix[ 0 ],
                         viewport_matrix[ 1 ],
                         viewport_matrix[ 2 ],
                         viewport_matrix[ 3 ] };
    glm::vec3 light_position_on_screen = glm::project ( light_pos, 
                                                        worldToView * model_matrix,
                                                        projection,
                                                        viewport );
    glm::vec2 normalized_light_pos;
    normalized_light_pos.x = light_position_on_screen.x / ( width * .5f )- 1.f;
    normalized_light_pos.y = light_position_on_screen.y / ( height * .5f )- 1.f;

    normalized_light_pos++;
    normalized_light_pos *= .5f;

    glFramebufferTexture2D ( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, out_pass, 0 );
    blur_shader.use ( );
    glActiveTexture ( GL_TEXTURE0 );
    glBindTexture ( GL_TEXTURE_2D, occlusion_pass );
    blur_shader.setUniform ( "firstPass", 0 );
    blur_shader.setUniform ( "texture_ratio", static_cast< float > ( width / height ) );
    blur_shader.setUniform ( "shaft_color1", glm::vec3 ( .716f, .730f, .805f ) );
    blur_shader.setUniform ( "lightOnScreen", normalized_light_pos );
    render_plane.draw ( );
    glEnable ( GL_DEPTH_TEST );   

    glBindFramebuffer ( GL_FRAMEBUFFER, 0 );
}


void Post_processor::target_size ( int new_width, int new_height )
{
    width  = new_width;
    height = new_height;
}