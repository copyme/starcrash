/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#include <cstring>
#include <stdexcept>
#include <algorithm>
#include <GL/glew.h>
#include <cassert>
#include <glm/gtc/matrix_transform.hpp>
#include "core/gbuffermanager.h"
#include "unitplane.h"
#include "glslprogram.h"

static GLSLProgram shader;
static void Paint ( std::pair< std::string, TexturedMesh * > mesh ) 
{
  shader.use();
  shader.setUniform ( "Projection", projection );
  shader.setUniform ( "View", worldToView );
  shader.setUniform ( "Diffuse", 0 );
//   shader.setUniform ( "Spec", 1 );
  mesh.second->draw();
  glUseProgram(0);
}


GBufferManager::GBufferManager()
{
  gbuffer_fbo[0] = 0;
  gbuffer_fbo[1] = 0;
  objects_register = nullptr;
  lights = nullptr;
  lightTarget = { 0.f, 0.f, 0.f };
  lightUp = { 0.f, 1.f, 0.f };
  memset ( gbufferTextures, 0, BUFFER_NUMBER * sizeof( GLuint ) );
}


void GBufferManager::init ( unsigned int width, unsigned int height )
{
  this->width = width;
  this->height = height;
  
  glGenTextures ( 5, gbufferTextures );
  // Create color texture
  glBindTexture(GL_TEXTURE_2D, gbufferTextures[0]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  
  // Create normal texture
  glBindTexture(GL_TEXTURE_2D, gbufferTextures[1]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  
  // Create depth texture
  glBindTexture(GL_TEXTURE_2D, gbufferTextures[2]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  
  // Create swap color texture
  glBindTexture(GL_TEXTURE_2D, gbufferTextures[3]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, width, height , 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  
  // Create depth texture -- depth from light perspective 
  glBindTexture(GL_TEXTURE_2D, gbufferTextures[4]);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  
  glGenFramebuffers ( 2, gbuffer_fbo );
  init_shader();
}

void GBufferManager::init_shader()
{
  bool status = true;
  status &= shader.compileShaderFromFile ( "shaders/basic.vert", GLSLShader::VERTEX );
  status &= shader.compileShaderFromFile ( "shaders/gbuffer.frag", GLSLShader::FRAGMENT );
  
  if ( !status )
  {
    throw std::runtime_error ( "Compilation of shaders faild: " + shader.log() );
  }
  
  shader.link ( );
  if ( shader.isLinked ( ) == false )
  {
    throw std::runtime_error ( "Linking of shaders faild: " + shader.log() );
  }
  
  status &= shader_point_light.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
  status &= shader_point_light.compileShaderFromFile ( "shaders/brdf_cook_torrance.frag", GLSLShader::FRAGMENT );
  
  if ( !status )
  {
    throw std::runtime_error ( "Compilation of shaders faild: " + shader_point_light.log() );
  }
  
  shader_point_light.link ( );
  if ( shader_point_light.isLinked ( ) == false )
  {
    throw std::runtime_error ( "Linking of shaders faild: " + shader_point_light.log() );
  }
  
  status &= shader_directional_light.compileShaderFromFile ( "shaders/gbuffer_debug.vert" , GLSLShader::VERTEX );
  status &= shader_directional_light.compileShaderFromFile ( "shaders/brdf_cook_torrance.frag", GLSLShader::FRAGMENT );
  
  if ( !status )
  {
    throw std::runtime_error ( "Compilation of shaders faild: " + shader_directional_light.log() );
  }
  
  shader_directional_light.link ( );
  if ( shader_directional_light.isLinked ( ) == false )
  {
    throw std::runtime_error ( "Linking of shaders faild: " + shader_directional_light.log() );
  }
}

void GBufferManager::geometry_pass()
{
  assert ( objects_register != nullptr );
  
  glDepthMask(GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  
  projection = glm::perspective ( 45.0f, ( float ) width / ( float ) height, .1f, 100000.f );
  worldToView = glm::lookAt( camera.eye, camera.o, camera.up);
  screenToWorld = glm::transpose(glm::inverse(projection * worldToView));
  
  //init Gbuffer for writing
  glBindFramebuffer ( GL_FRAMEBUFFER, gbuffer_fbo[0] );
  // Attach textures to framebuffer
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, gbufferTextures[0], 0);
  gbufferDrawBuffers[0] = GL_COLOR_ATTACHMENT0;
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1 , GL_TEXTURE_2D, gbufferTextures[1], 0);
  gbufferDrawBuffers[1] = GL_COLOR_ATTACHMENT1;
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, gbufferTextures[2], 0);
  
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  {
    throw std::runtime_error ( "Framebuffer coruppted!" );
  }
  
  glDrawBuffers ( 2, gbufferDrawBuffers );
  
  glClear ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  
  std::for_each ( objects_register->begin(), objects_register->end(), Paint );
}

void GBufferManager::shadowmap_pass( Light const & light)
{
  glDepthMask(GL_TRUE);
  glEnable(GL_DEPTH_TEST);
  glBindFramebuffer ( GL_FRAMEBUFFER, gbuffer_fbo[1] );
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT , GL_TEXTURE_2D, gbufferTextures[4], 0);
  glDrawBuffers (0, 0);
  
  glClear(GL_DEPTH_BUFFER_BIT);
  
  glm::vec3 lightDirection = glm::normalize(lightTarget - light.position);
  glm::mat4 worldToLight = glm::lookAt(light.position, lightDirection, lightUp);
  
  projection = ligthToShadowMap;
  worldToView = worldToLight;
  std::for_each ( objects_register->begin(), objects_register->end(), Paint );
  glDepthMask(GL_FALSE);
  glDisable ( GL_DEPTH_TEST );
}

void GBufferManager::light_pass()
{
  assert ( lights != nullptr );
  static GLfloat broom [] = { 0.0f, 0.0f, 0.0f, 0.0f };
  static UnitPlane render_plane;
  
  //Clean unbind textures and bind secound one for lights
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, gbufferTextures[3], 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1 , GL_TEXTURE_2D, 0, 0);
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT , GL_TEXTURE_2D, 0, 0);
  
  glDrawBuffers ( 1, &gbufferDrawBuffers[0] );
  
  glClearBufferfv (GL_COLOR, 0, broom); // Fsck glClear does not work! I hate it and love this glClearBufferfv :P
  
  glEnable (GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_ONE, GL_ONE);
  for ( register unsigned int i = 0; i < lights->size(); i++ )
  {
    Light light = lights->at ( i );
    shadowmap_pass(light);
    glBindFramebuffer ( GL_FRAMEBUFFER, gbuffer_fbo[0] );
    glDrawBuffers ( 1, &gbufferDrawBuffers[0] );
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, gbufferTextures[3], 0);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, gbufferTextures[0]);
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, gbufferTextures[1]);
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, gbufferTextures[2]);
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, gbufferTextures[4]);
    glm::vec3 lightDirection = glm::normalize ( lightTarget - light.position );
    glm::mat4 worldToLight = glm::lookAt ( light.position, lightDirection, lightUp );
    worldToShadowMap =  MAT4F_M1_P1_TO_P0_P1 * ligthToShadowMap * worldToLight;
    
    if( light.type == Light::POINT )
      use_point_light_shader( light );
    render_plane.draw();
  }
  glDisable ( GL_BLEND );
}

void GBufferManager::use_point_light_shader( Light &light )
{
  shader_point_light.use();
  shader_point_light.setUniform("diffuse", 0);
  shader_point_light.setUniform("Normal", 1);
  shader_point_light.setUniform("Depth", 2);
  shader_point_light.setUniform("Shadow", 3);
  shader_point_light.setUniform("InverseViewProjection",  screenToWorld);
  
  shader_point_light.setUniform("ProjectionLight", worldToShadowMap);
  shader_point_light.setUniform("lightPos", light.position );
  shader_point_light.setUniform("specular_color", light.color);
}


void GBufferManager::envmap_pass()
{
  glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D, gbufferTextures[0], 0);
  
  glDrawBuffers ( 1, &gbufferDrawBuffers[0] );
  
  glClear(GL_COLOR_BUFFER_BIT);
  
  projection = glm::perspective ( 45.0f, ( float ) width / ( float ) height, .1f, 100000.f );
  worldToView = glm::lookAt( camera.eye, camera.o, camera.up);
  env->render ( projection, worldToView );
}

void GBufferManager::deactivate()
{
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

GBufferManager::~GBufferManager()
{
  if ( gbuffer_fbo[0] != 0 )
  {
    glDeleteTextures ( BUFFER_NUMBER, gbufferTextures );
    glDeleteFramebuffers ( 2, gbuffer_fbo );
  }
}
