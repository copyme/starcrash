/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#ifndef UNITPLANE_H
#define UNITPLANE_H
#include <GL/glew.h>

class UnitPlane
{
private:
  GLuint vao;
  static const int quad_triangleCount; 
  GLuint vbo[2];
public:
  UnitPlane();
  void draw();
};

#endif // UNITPLANE_H
