/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#ifndef LIGHT_H
#define LIGHT_H
#include <glm/glm.hpp>
#include <glm/detail/type_vec.hpp>

struct Light
{
  enum TYPE { POINT };
  TYPE type;
  glm::vec3 position;
  glm::vec3 color;
  float intensity;
};

#endif // LIGHT_H
