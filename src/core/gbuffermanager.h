/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */


/**
 * \todo Light manager support
 * 
 */


#ifndef GBUFFERMANAGER_H
#define GBUFFERMANAGER_H

#include <GL/gl.h>
#include <map>
#include <vector>
#include "texturedmesh.h"
#include "environment.h"
#include "light.h"

class GBufferManager
{
private:
    static const int BUFFER_NUMBER = 5;
    GLuint gbufferTextures[BUFFER_NUMBER];
    GLuint gbufferDrawBuffers[2]; // Color & Norlams
    GLuint gbuffer_fbo[2];
    glm::vec3 lightTarget;
    glm::vec3 lightUp;
    void init_shader();
    unsigned int width;
    unsigned int height;
    Environment * env;
    GLSLProgram shader_point_light;
    GLSLProgram shader_directional_light;
    glm::mat4 worldToShadowMap;
    void use_point_light_shader( Light &light );
    std::map< std::string, TexturedMesh * > * objects_register;
    std::vector< Light > * lights;
    void shadowmap_pass( Light const & light);
public:
    GBufferManager();
    void init ( unsigned int width, unsigned int height );
    void set_geometry ( std::map< std::string, TexturedMesh * > * objects_register ) { this->objects_register = objects_register; }
    void set_lights ( std::vector < Light > * lights ) { this->lights = lights; }
    void set_envmap ( Environment * env ) { this->env = env; } 
    void geometry_pass();
    void light_pass();
    void envmap_pass();
    void deactivate();
    GLuint get_render() const { return gbufferTextures[3]; }
    GLuint get_Z() const { return gbufferTextures[2]; }
    GLuint get_envmap_render() const { return gbufferTextures[0]; }
    ~GBufferManager();
};

#endif // GBUFFERMANAGER_H
