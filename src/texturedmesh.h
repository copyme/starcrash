/*
 * **"THE COFFEEWARE LICENSE" (Revision 2):**
 *
 * <Kacper Pluta> wrote this code. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a coffee in return.
 */

#ifndef TEXTUREDMESH_H
#define TEXTUREDMESH_H

#include <vector>
#include <tiny_obj_loader.h>
#include "gltexture.h"
#include "indexed_mesh.hpp"

class TexturedMesh
{
private:
  std::vector < AbstractMesh * > glMesh;
  std::vector < GLTexture * > textures;
public:
  void add_geometry ( std::vector < tinyobj::shape_t > & shape );
  void add_mesh ( AbstractMesh * mesh ) { glMesh.push_back( mesh ); }
  void add_texture ( char const * file, GLenum texture_id );
  void draw ();
  ~TexturedMesh();
};

#endif // TEXTUREDMESH_H